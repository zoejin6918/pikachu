package pikachu;

import java.util.ArrayList;

public class MovingAverage
{
    private int term_length;
    private ArrayList<Double> data;
    
    public MovingAverage(int length) {
    	this.data = new ArrayList<Double>();
    	this.term_length = length;
    }
    
    public void addNumber(double[] numbers) {
        for (double n : numbers) {
            if (data.size() >= getTermLength()) {
              data.remove(0) ; 
            } 
            data.add(n);
        }
    }
    
    public void addNumber(double currentPrice) {
    	 if (data.size() >= getTermLength()) {
             data.remove(0); 
          } 
          data.add(currentPrice);
    }
    public int getTermLength() {
    	return this.term_length;
    }
    
    public double calculate() {
        double total = 0;
        if (data.size() < getTermLength()) {
        	return 0;
        }
        else {
        	for (double d : data ) {
        		total += d;
        	}
        	return total / data.size();
        }
    }
    
}
