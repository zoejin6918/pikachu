package pikachu;

public class TwoMAStrategy {
	private int ticker;
	private int flag = 0;
	private int previousFlag;
	private String strategy;
	
	private MovingAverage longTerm;
	private MovingAverage shortTerm;
	
	public TwoMAStrategy(int length_long, int length_short) {
		this.ticker = 0;
		this.longTerm = new MovingAverage(length_long);
		this.shortTerm = new MovingAverage(length_short);
		strategy = "N/A";
	}
	
	public void twoMAStrategy(double currentPrice) {
		this.ticker++;
		String buyOrSell;
		longTerm.addNumber(currentPrice);
		shortTerm.addNumber(currentPrice);
		
		double shortAvg = shortTerm.calculate();
		double longAvg = longTerm.calculate();
		
		if (shortAvg != 0 && longAvg != 0) {
			if (flag == 0) {
				flag = shortAvg > longAvg ? 1 : -1;
				previousFlag = flag;
			}
			else {
				flag =  shortAvg > longAvg ? 1 : -1;
				if (flag != previousFlag) {
					buyOrSell = flag > previousFlag ? "BUY" : "SELL";
					strategy = buyOrSell + " at Price=" + currentPrice + ", ticker=" + ticker;
					System.out.println(strategy);
				} 
				previousFlag = flag;
			}
		}
	}
	
	public double getLongTermAvg() {
		return longTerm.calculate();
	}
	
	public double getShortTermAvg() {
		return shortTerm.calculate();
	}
	
	public String getStrategy() {
		return this.strategy;
	}
}
