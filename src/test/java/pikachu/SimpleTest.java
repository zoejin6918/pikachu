package pikachu;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

public class SimpleTest {
	private MovingAverage ma;
	private TwoMAStrategy twoMA;
	
	@Test
	public void testMA() {
		double[] input1 = {2, 2, 2, 4, 4, 4};
		ma.addNumber(input1);
		double result = ma.calculate();
		System.out.println(result);
		assertEquals(0, result, 0);
		
		double[] input2 = {1, 2, 3, 4, 5 ,6 ,7 , 8, 9, 10,
							1, 2, 3, 4, 5 ,6 ,7 , 8, 9, 10,
							1, 2, 3, 4, 5 ,6 ,7 , 8, 9, 10};
		ma.addNumber(input2);
		result = ma.calculate();
		System.out.println(result);
		assertEquals(5.5, result, 0);
	}
	
	@Test
	public void testTwoMA() {
		twoMA.twoMAStrategy(11);
		twoMA.twoMAStrategy(10);
		twoMA.twoMAStrategy(11);
		twoMA.twoMAStrategy(10);
		twoMA.twoMAStrategy(11);
		twoMA.twoMAStrategy(10);
		System.out.println(twoMA.getStrategy ());
		twoMA.twoMAStrategy(11);
		twoMA.twoMAStrategy(10);
		twoMA.twoMAStrategy(9);
		twoMA.twoMAStrategy(10);
		twoMA.twoMAStrategy(11);
		twoMA.twoMAStrategy(13);
		assertEquals("BUY at Price=13.0, ticker=12", twoMA.getStrategy());
		assertEquals(10.75, twoMA.getShortTermAvg(), 0);
		assertEquals(10.60, twoMA.getLongTermAvg(), 0);
		
		twoMA.twoMAStrategy(14);
		assertEquals("BUY at Price=13.0, ticker=12", twoMA.getStrategy());
	}
	
	@Before
	public void initial() {
		ma = new MovingAverage(30);
		twoMA = new TwoMAStrategy(10, 4);
	}

}
